QA Exercises Details

CodeReview.java : (Code Review) Updated code with fixes for 3 issues in the java file.

SpecReviewQuestions.rtf  : (Spec Review) Set of questions for the specs.

JiraAutomation/ folder : 

Java project implementing JIRA "create Issue" scenario using selenium and page object patern.Test has been written against Altassina public instance, 
https://jira.atlassian.com/browse/TST/.Running test JiraTest will execute the scenario.

Note: 
1. Selenium JARs have been added to the repo under lib/ folder and dependency is updated.
2. Chrome driver is also bundled and  added to resources folder.
3. Project was built using JDK 1.6 on Intellij Idea.