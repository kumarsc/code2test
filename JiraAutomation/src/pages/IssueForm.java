package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Sushant on 2/12/15.
 */
public class IssueForm {

    private final WebDriver driver;
    private  String issueName;

    public IssueForm(WebDriver driver) {
        this.driver = driver;
//        System.out.println("Title is:" + driver.getTitle());
        if (!"A Test Project - Atlassian JIRA".equals(driver.getTitle())) {
            throw new IllegalStateException("Issue form not found");
        }
    }

    /**
     * Refactoring required to add utility class to parse locators from properties file
     */

    By summary = By.id("summary");
    By security = By.id("security");
    By assigneeDropDown = By.cssSelector("#assignee-single-select span");


    public IssuePage FillAndSubmit() {


        driver.findElement(summary).sendKeys("Jira Automation Scenario");

        WebElement securityLevel=driver.findElement(security);
        Select se = new Select(securityLevel);
        se.selectByValue("10032");

        driver.findElement(assigneeDropDown).click();
        driver.findElement(By.cssSelector(".aui-list-item-link.aui-iconised-link")).click();


        driver.findElement(By.id("create-issue-submit")).click();

        WebElement issueLink = (new WebDriverWait(driver, 20))
                .until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".issue-created-key.issue-link")));

        issueLink.click();

        WebElement issueDetail= (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.presenceOfElementLocated(By.id("key-val")));

        return new IssuePage(driver);
    }

    public String getIssueName() {
        return  issueName;
    }

}
