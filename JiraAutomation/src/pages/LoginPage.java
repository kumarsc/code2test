package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.concurrent.TimeUnit;

/**
 * Created by Sushant on 2/11/15.
 */
public class LoginPage {

    private final WebDriver driver;

    public LoginPage(WebDriver driver) {

        this.driver = driver;
        navigateToLogin();
        if (!"Sign in to continue".equals(driver.getTitle())) {
            throw new IllegalStateException("login page not found");
        }
    }
        By email = By.id("username");
        By pwd = By.id("password");
        By submitButton = By.id("login-submit");

    //Method called inside constructor, declared final to avoid method override
    public final LoginPage navigateToLogin() {
        WebElement element;
        element = driver.findElement(By.cssSelector(".aui-nav-link.login-link"));
        element.click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return this;
    }


    public LoginPage typeEmail(String emailAddress) {
            driver.findElement(email).sendKeys(emailAddress);
            return this;
        }

    public LoginPage typePassword(String password) {
        driver.findElement(pwd).sendKeys(password);
        return this;
    }

    public JiraHome submitLogin() {
        driver.findElement(submitButton).submit();
        return new JiraHome(driver);
    }

    public JiraHome loginAs(String emailAddress, String password) {
        typeEmail(emailAddress);
        typePassword(password);
        return submitLogin();
    }





}
