package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by Sushant on 2/12/15.
 */
public class IssuePage {

    private final WebDriver driver;

    public IssuePage(WebDriver driver) {
        this.driver = driver;
//        System.out.println("Title is:" + driver.getTitle());
        if (!driver.getTitle().contains("Jira Automation Scenario - Atlassian JIRA")) {
            throw new IllegalStateException("Issue details page not found");
        }
    }

    public String getIssueID(){

        return driver.findElement(By.id("key-val")).getText();
    }

    public String getIssueSummary (){

        return driver.findElement(By.id("summary-val")).getText();
    }


}
