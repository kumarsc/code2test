package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Sushant on 2/11/15.
 */
public class JiraHome {

    private final WebDriver driver;

    By createIssue = By.id("create_link");

    public JiraHome(WebDriver driver) {
        this.driver = driver;
//        System.out.println("Title is:" + driver.getTitle());
        if (!"A Test Project - Atlassian JIRA".equals(driver.getTitle())) {
            throw new IllegalStateException("home page not found");
        }
    }


    public IssuePage CreateIssue() throws InterruptedException{

        launchNewIssue();
        IssueForm NewIssue = new IssueForm(driver);
        return NewIssue.FillAndSubmit();

    }

    public IssueForm launchNewIssue() {
        driver.findElement(createIssue).click();

        //wait for create issue form
        WebElement loader = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOfElementLocated(By.className("jira-page-loading-indicator")));

        WebElement element = driver.switchTo().activeElement();
        if (!"Create".equals(element.getText())) {
            throw new IllegalStateException("create issue dialog not found");
        }

        return new IssueForm(driver);

    }

}
