package tests;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.IssuePage;
import pages.JiraHome;
import pages.LoginPage;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class JiraTest {

public static WebDriver driver;

private static final String Email= "sushant.choudhary@hotmail.com"  ;
private static final String Pwd= "Primaver@1";
LoginPage login;
JiraHome home;
IssuePage issue;



@BeforeClass
public static void prepUp()
    {

    String path= (System.getProperty("user.dir")) + "/resources/chromedriver";
	System.setProperty("webdriver.chrome.driver", path);
	driver = new ChromeDriver();
    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    driver.get("https://jira.atlassian.com/browse/TST/");
    }

@AfterClass
    public static void cleanUp()
	{
		driver.quit();
	}

@Test
public void createIssue() throws InterruptedException
	{
    login= new LoginPage(driver);
    login.loginAs(Email,Pwd);

    home= new JiraHome(driver);
    home.CreateIssue();

    issue = new IssuePage(driver);

    //Assertion
    assertTrue("Failed to create a new issue id", issue.getIssueID().contains("TST"));
    assertEquals("Failed to create a new issue summary", issue.getIssueSummary(),"Jira Automation Scenario");





}
}
