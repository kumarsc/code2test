public ArrayList<Issue> addWatcherToAll (final ArrayList<Issue> issues, final User
currentUser, final User watcher) {
    ArrayList<Issue> successfulIssues = new ArrayList<Issue> ();
    ArrayList<Issue> failedIssues = new ArrayList<Issue> ();
    for (Issue issue : issues) {
        if (canWatchIssue(issue, currentUser, watcher)) {
            successfulIssues.add (issue);
}
else {
            failedIssues.add (issue);
        }
    }
    /**
     * If condition below was checking for empty list and performing action on elements
     * Added a not condition in the statement
     */
    if (!successfulIssues.isEmpty()) {
    	/**
    	 * As per spec, we need to add watcher to issues and watcher can be different than current user,
    	 * updated the method call below with "watcher" param instead of "currentUser".This would also be true
    	 * in case currentUser.equals(watcher)
    	 */
        watcherManager.startWatching (watcher, successfulIssues);
    }
      /**
       * Assuming watcher is class attribute for an issue, updated(commented block) the code below to apply startWatching to an issue
       * object instead of list of issues.This is just an enhancement though in case assumption holds true.
       */
//    if (!successfulIssues.isEmpty()) {
//    	for (Issue issue : successfulIssues) {
//    		watcherManager.startWatching (watcher, issue);
//    	}
//        
//    }
    return failedIssues;
}
private boolean canWatchIssue (Issue issue, User currentUser, User watcher) {
    /**
     * Condition below assumes current user can check its watcher status, 
     * we might need a getHasPermissionToModifyWatchers check even if they are same user.
     */
	if (currentUser.equals(watcher) || currentUser.getHasPermissionToModifyWatchers()) {
        return issue.getWatchingAllowed (watcher);
    }
    /**
     * return statement below was always true even if condition above has failed, updated it to false
     */
    return false
}
￼￼